<?php

namespace ISMiddleWareApp;

require_once(__DIR__ . '/../../vendor/issdk/Infusionsoft/infusionsoft.php');

use Symfony\Component\HttpFoundation\Request;
use Curl\Curl;


class InfusionToFive9
{
    /**
     * @var string
     */
    private $five9APIEndPoint;

    /**
     * @var Infusionsoft_Contact
     */
    private $contact;

    /**
     * @var Infusionsoft_Lead
     */
    private $opportunity;

    /**
     * @var Infusionsoft_Stage
     */
    private $stage;

    /**
     * @var array|int
     */
    private $queryData = array();

    /**
     * Construct
     */
    public function __construct()
    {
        $this->five9APIEndPoint = 'https://api.five9.com/web2campaign/AddToList';

        // Gather and organize data from the parameters send in the url query string
        $queryStr = Request::createFromGlobals()->getQueryString();
        parse_str($queryStr, $queryData);

        $this->queryData = $queryData;

        // Load the Contact.
        $this->contact = new \Infusionsoft_Contact( $this->get('contactId') );


        // Search the opportunity stage info so we can use his ID and Name later.
        $searchStage = \Infusionsoft_DataService::query( new \Infusionsoft_Stage(), array( 'StageName' => $this->get('salesStage') ) );
        $this->stage = array_shift( $searchStage );


        // Search a unique Opportunity tied to the Contact by ContactID and StageID which are sent on the url query string. Normally this would be "New Opportunity" (StageID => 18).
        $searchOpportunity = \Infusionsoft_DataService::query( new \Infusionsoft_Lead(), array( 'ContactID' => $this->contact->Id, 'StageID' => $this->stage->Id ) );
        $this->opportunity = array_shift( $searchOpportunity );

    }


    public function get($index)
    {
        if (array_key_exists($index, $this->queryData))
            return $this->queryData[$index];

        return false;
    }


    /**
     * @return array
     */
    public function sendToFive9()
    {
        $curlFive9 = new Curl();

        $curlFive9->post( 
            $this->five9APIEndPoint, array(
                'F9domain' => $this->get('F9domain'),
                'F9list' => $this->get('F9list'),
                'Contact ID' => $this->contact->Id,
                'number1' => $this->contact->Phone1,
                'number2' => $this->contact->Phone2,
                'first_name' => $this->contact->FirstName,
                'last_name' => $this->contact->LastName,
                'company' => $this->contact->Company,
                'Job Title' => $this->contact->JobTitle,
                'street' => $this->contact->StreetAddress1,
                'Address 2' => $this->contact->StreetAddress2,
                'city' => $this->contact->City,
                'state' => $this->contact->State,
                'zip' => $this->contact->PostalCode,
                'Country' => $this->contact->Country,
                'Email' => $this->contact->Email,
                'Opportunity ID' => $this->opportunity->Id,
                'Recent_Disposition' => $this->opportunity->_Five9Disposition,
                'Sales Stage' => $this->stage->StageName,
                'Lead Form' => $this->get('formName'),
                'Owner ID' => $this->contact->OwnerID,
                'F9CallASAP' => 1
                )
);

return $curlFive9;
}

}