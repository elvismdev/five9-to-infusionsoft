<?php

require __DIR__ . '/vendor/autoload.php';

use ISMiddleWareApp\Five9ToInfusion;



$app = new Five9ToInfusion();

// Contact
if ( $app->updateContact() === true )
	echo "> Contact record has changes...<br>";
else
	echo "> No changes for Contact record...<br>";



// Opportunity
if ( $app->updateOpportunity() === true )
	echo "> Opportunity record has changes...<br>";
else
	echo "> No changes for Opportunity record...<br>";



// Note
if ( $app->addNote() === true )
	echo "> 1 New Note to Add...<br>";
else
	echo "> No love :(...<br>";




echo $app->isSuccess();